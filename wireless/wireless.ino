#include <Arduino.h>
// #include <math.h>

#define PWM_CHANNEL_0 0
#define PWM_CHANNEL_4 g_APinDescription[9].ulPWMChannel
#define PWM_CHANNEL_5 g_APinDescription[8].ulPWMChannel
#define PWM_CHANNEL_6 g_APinDescription[7].ulPWMChannel
#define PWM_CHANNEL_7 g_APinDescription[6].ulPWMChannel

// The highest stable frequency that can be generated using an Arduino Due is 84 MHz
uint32_t pwm_clk = 42000000ul;
uint32_t pwm_period = 4200; // (1/4200)*(pwm_clk) = (1/4200)*84MHz = 20kHz   (max value 65535)
float Ts = 1 / 20000.;
float duty = 0;
uint32_t val1;
uint32_t val2;
uint32_t x = 0;
uint32_t d1;
uint32_t d2;

float scale_factor = 10. * 3.3 ; // Differential Op-amp ratio 100k:10k
float duty_tmp = 0;

//////////////////////////////////////
// PI Controller
float V_sen;
float V_in;
float V_ref = 0;
float V_ref_set = 3.;
float slope = 0.001;
float V_sat_max = 0.7;
float V_sat_min = 0;
int flag_run = 0;
//
float V_err = 0;
float V_anti = 0;
float V_p = 0;
float V_integ = 0;
float V_pi = 0;
float V_out = 0;
// Gain
float Kp = 0.05;
float Ki = 200.;
float Ka = 1 / Kp;

//////////////////////////////////////

void setup() {
  //Serial.begin(115200);
  //while (! Serial);
  ADC_setup();
  DAC_setup();
  IO_setup();
  PWM_setup();
}

void loop() {
  //Serial.print("duty =");
  //Serial.println(duty);
  //Serial.println("\n");
}


void PWM_Handler(void) {

   ///////////////////////////AD setup//////////////////////////
  while ((adc_get_status(ADC) & ADC_ISR_DRDY) != ADC_ISR_DRDY)
  {}; //Wait for end of conversion
  val1 = adc_get_channel_value(ADC, ADC_CHANNEL_7); // Read ADC (Channel 7 = port 0)
  V_in = val1;
  // V_in = (val1* pwm_period*10000) / 4096;
  //val2 = adc_get_channel_value(ADC, ADC_CHANNEL_6);
  //val2 = (val2 * pwm_period) / 4096;

  //////////////////////Algorithm//////////////////////////
  // PI controller - GCL 20170404

  if (abs(V_ref_set - V_ref) > slope) {
    V_ref = V_ref +  (V_ref_set - V_ref) / abs(V_ref_set - V_ref) * slope;
  }

  V_sen = scale_factor * V_in / 4096 - 0.125;

  if (V_sen > V_ref_set * 1.5) {
    flag_run = 1;
  }

  if (flag_run == 1) {

    V_err = V_ref - V_sen;
    V_anti = Ka * (V_pi - V_out);
    V_p = Kp * V_err;
    V_integ += Ki * (V_err - V_anti) * Ts;

    V_pi = V_p + V_integ;

    //Limiting
    if (V_pi < V_sat_max && V_pi > 0) {
      V_out = V_pi;
    }
    else if (V_pi > V_sat_max) {
      V_out = V_sat_max;
    }
    else {
      V_out = V_sat_min;
    }

    /*
      duty_tmp = duty_tmp + 1;
      duty = duty_tmp/100;
      if(duty_tmp > 90) {
       duty_tmp = 0.;
      }
    */
    duty = V_out;
  }
  else {
    duty = 0.4;
  }

  ////////////////////// PWM_duty out ///////////////////////////
  d1 = pwm_period * duty;
  d2 = pwm_period * (1 - duty);
  PWMC_SetDutyCycle (PWM, PWM_CHANNEL_0, d1);

  ////////////////////// DA out ///////////////////////////
  dacc_set_channel_selection(DACC, 1);
  V_sen = 1.7;
  dacc_write_conversion_data(DACC, ((V_sen-0.5) * 4096/2.2));

}

void IO_setup() {
  int ulPin = 8;
  // Setup PWM5L (pin 8)
  PIO_Configure (g_APinDescription[ulPin].pPort,
                 g_APinDescription[ulPin].ulPinType,
                 g_APinDescription[ulPin].ulPin,
                 g_APinDescription[ulPin].ulPinConfiguration);

  ulPin = 9;
  // Setup PWM4L (pin 9)
  PIO_Configure (g_APinDescription[ulPin].pPort,
                 g_APinDescription[ulPin].ulPinType,
                 g_APinDescription[ulPin].ulPin,
                 g_APinDescription[ulPin].ulPinConfiguration);

  ulPin = 6;
  // Setup PWM7L (pin 6)
  PIO_Configure (g_APinDescription[ulPin].pPort,
                 g_APinDescription[ulPin].ulPinType,
                 g_APinDescription[ulPin].ulPin,
                 g_APinDescription[ulPin].ulPinConfiguration);

  ulPin = 7;
  // Setup PWM6L (pin 7)
  PIO_Configure (g_APinDescription[ulPin].pPort,
                 g_APinDescription[ulPin].ulPinType,
                 g_APinDescription[ulPin].ulPin,
                 g_APinDescription[ulPin].ulPinConfiguration);

  // Setup PWM0L (pin 34)
  PIOC -> PIO_PDR = 1 << 2;
  PIOC -> PIO_IDR = 1 << 2;
  PIOC -> PIO_ABSR |= 1 << 2;

  // Setup PWM5H (pin 44)
  PIOC -> PIO_PDR = 1 << 19;
  PIOC -> PIO_IDR = 1 << 19;
  PIOC -> PIO_ABSR |= 1 << 19;

  // Setup PWM6H (pin 45)
  PIOC -> PIO_PDR = 1 << 18;
  PIOC -> PIO_IDR = 1 << 18;
  PIOC -> PIO_ABSR |= 1 << 18;

  // Setup PWM0H (pin 35)
  PIOC -> PIO_PDR = 1 << 3;
  PIOC -> PIO_IDR = 1 << 3;
  PIOC -> PIO_ABSR |= 1 << 3;

  //ADC channel
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);

  //DAC channel
  PIOB->PIO_PDR = PIO_PB25B_TIOA0 ;  // disable PIO control
  PIOB->PIO_IDR = PIO_PB25B_TIOA0 ;   // disable PIO interrupts
  PIOB->PIO_ABSR |= PIO_PB25B_TIOA0 ;  // switch to B peripheral

}

void PWM_setup ()
{
  uint16_t DB = 100;  // Dead time 100*(1/84,000,000) = 1.19us
  //PWM config
  pmc_enable_periph_clk (ID_PWM);

  PWMC_DisableChannel (PWM, PWM_CHANNEL_4);
  PWMC_DisableChannel (PWM, PWM_CHANNEL_5);
  PWMC_DisableChannel (PWM, PWM_CHANNEL_6);
  PWMC_DisableChannel (PWM, PWM_CHANNEL_0);

  PWMC_ConfigureClocks (pwm_clk, 0, VARIANT_MCK);   //VARIANT_MCK = 84000000 in /variant.h

  PWMC_ConfigureSyncChannel (PWM,
                             PWM_SCM_SYNC4 | PWM_SCM_SYNC5 | PWM_SCM_SYNC6 | PWM_SCM_SYNC7 | PWM_SCM_SYNC0
                             , PWM_SCM_UPDM_MODE1, 0, 0);

  PWMC_SetPeriod (PWM, PWM_CHANNEL_0, pwm_period);
  PWMC_SetPeriod (PWM, PWM_CHANNEL_4, pwm_period);
  PWMC_SetPeriod (PWM, PWM_CHANNEL_5, pwm_period);
  PWMC_SetPeriod (PWM, PWM_CHANNEL_6, pwm_period);
  PWMC_SetPeriod (PWM, PWM_CHANNEL_7, pwm_period);

  //PWM configure (pwm, channel, prescaler, alignment, polartiy, eventselect, dead time enable, DTH, DTL)
  PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_0, 0, 0, 0, 0, 1 << 16, 0, 0); //left alignment
  //PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_0, 0, 1 << 8, 0, 0, 1 << 16, 0, 0); //center alignment (synchronize all other pwms)
  //PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_0, 0,1<<8,0,1<<10,1<<16,0,0); //center alignment double sampling( synchronize all other pwms)
  PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_4, 1, 0, 0, 0, 1 << 16, 0, 0);
  PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_5, 1, 0, 0, 0, 1 << 16, 0, 0);
  PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_6, 1, 0, 0, 0, 1 << 16, 0, 0);
  PWMC_ConfigureChannelExt (PWM, PWM_CHANNEL_7, 1, 0, 0, 0, 1 << 16, 0, 0);

  PWMC_SetDeadTime(PWM, PWM_CHANNEL_0, DB, DB);
  PWMC_SetDeadTime(PWM, PWM_CHANNEL_5, DB, DB);
  PWMC_SetDeadTime(PWM, PWM_CHANNEL_6, DB, DB);

  PWMC_SetDutyCycle (PWM, PWM_CHANNEL_0, 4200);
  PWMC_SetDutyCycle (PWM, PWM_CHANNEL_4, 4200);
  PWMC_SetDutyCycle (PWM, PWM_CHANNEL_5, 4200);
  PWMC_SetDutyCycle (PWM, PWM_CHANNEL_6, 4200);
  PWMC_SetDutyCycle (PWM, PWM_CHANNEL_7, 4200);

  PWMC_SetSyncChannelUpdatePeriod (PWM, 1);

  PWMC_EnableChannel (PWM, PWM_CHANNEL_0);
  PWMC_EnableChannelIt(PWM, PWM_CHANNEL_0);
  PWMC_SetSyncChannelUpdateUnlock (PWM);
  NVIC_EnableIRQ(PWM_IRQn);
}

void ADC_setup() {

  pmc_enable_periph_clk(ID_ADC); // To use peripheral, we must enable clock distributon to it
  adc_init(ADC, SystemCoreClock, ADC_FREQ_MAX, ADC_STARTUP_FAST); // initialize, set maximum posibble speed
  adc_disable_interrupt(ADC, 0xFFFFFFFF);
  adc_set_resolution(ADC, ADC_12_BITS);
  adc_configure_power_save(ADC, 0, 0); // Disable sleep
  adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1); // Set timings - standard values
  adc_set_bias_current(ADC, 1); // Bias current - maximum performance over current consumption
  adc_stop_sequencer(ADC); // not using it
  adc_disable_tag(ADC); // it has to do with sequencer, not using it
  adc_disable_ts(ADC); // deisable temperature sensor
  adc_disable_channel_input_offset(ADC, ADC_CHANNEL_6);

  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_7);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_6);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_5);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_4);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_3);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_2);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_1);
  adc_disable_channel_differential_input(ADC, ADC_CHANNEL_0);

  adc_configure_trigger(ADC, ADC_TRIG_SW, 1); // triggering from software, freerunning mode
  adc_disable_all_channel(ADC);
  adc_enable_channel(ADC, ADC_CHANNEL_7); // channel 7 enable (port0 = ADC Channel 7)
  adc_enable_channel(ADC, ADC_CHANNEL_6);
  // adc_enable_channel(ADC, ADC_CHANNEL_5);
  // adc_enable_channel(ADC, ADC_CHANNEL_4);
  // adc_enable_channel(ADC, ADC_CHANNEL_3);
  // adc_enable_channel(ADC, ADC_CHANNEL_2);
  // adc_enable_channel(ADC, ADC_CHANNEL_1);
  // adc_enable_channel(ADC, ADC_CHANNEL_0);

  adc_start(ADC);
}


void DAC_setup() {
  pmc_enable_periph_clk(ID_DACC);
  dacc_reset(DACC);
  dacc_disable_trigger(DACC);
  dacc_set_transfer_mode(DACC, 0);
  dacc_enable_flexible_selection(DACC);
  dacc_enable_channel(DACC, 0);
  dacc_enable_channel(DACC, 1);
}
